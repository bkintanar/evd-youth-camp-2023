<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $validated = $request->validate([
            'first_name' => ['required'],
            'last_name' => ['required'],
            'middle_initial' => ['nullable'],
            'email' => ['required', 'unique:users'],
            'section' => ['required'],
            'gender' => ['required'],
            'local_church' => ['required'],
            'host_pastor' => ['required'],
            'question' => ['nullable'],
            'proof_of_payment' => ['required']
        ]);

        $validated['name'] = $validated['last_name'] . ', ' . $validated['first_name'] . ' ' . $validated['middle_initial'];

        if($validated['proof_of_payment']) {
            $file = $validated['proof_of_payment'];
            $fileName = time().'_'.$file->getClientOriginalName();
            $filePath = $request->file('proof_of_payment')->storeAs('uploads', $fileName, 'public');
            $validated['proof_of_payment'] = '/storage/' . $filePath;
        }

        User::create($validated);

        return response()->json(['message'=> 'Success'], Response::HTTP_CREATED);
    }
}
