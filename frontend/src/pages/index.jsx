import Head from 'next/head'

import { Footer } from '@/components/Footer'
import { Header } from '@/components/Header'
import { Hero } from '@/components/Hero'
import { Newsletter } from '@/components/Newsletter'
import { Speakers } from '@/components/Speakers'
import { RegistrationForm } from '@/components/RegistrationForm'
import Image from 'next/image'
import background from '@/images/background-evd.png'

export default function Home() {
  return (
    <>
      <Head>
        <title>UPCPI EVD Youth Camp 2023 Registration</title>
        <meta
          name="description"
          content="The wait is finally over! The online registration for the UPCPI EVD Youth Camp 2023 is finally open."
        />
      </Head>
      <div className='relative'>
        <Image
          src={background}
          alt='evd'
          quality={100}
          className=" fixed"
        />
        <div className='relative'>
        <Header />
          <main>
            <Hero />
            <Speakers />
            <RegistrationForm />
            <Newsletter />
          </main>
          <Footer />
        </div>
      </div>
    </>
  )
}
