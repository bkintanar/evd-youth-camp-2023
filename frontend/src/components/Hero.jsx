import Image from 'next/image'

import { Button } from '@/components/Button'
import { Container } from '@/components/Container'
import backgroundImage from '@/images/background.jpg'
import backgroundImage2 from '@/images/2.jpg'

export function Hero() {
  return (
    <div className="relative pt-10 pb-20 sm:py-24">
      <Container className="relative">
        <div className="mx-auto max-w-2xl lg:max-w-4xl lg:px-12">
          <h1 className="font-display text-5xl font-bold tracking-tighter text-white sm:text-7xl">
            <span className="sr-only"></span>UPCPI EVD Youth Camp 2023 Registration
          </h1>
          <div className="mt-6 space-y-6 font-display text-2xl tracking-tight text-gray-300">
            <p>
              The wait is finally over! The online registration for the UPCPI EVD Youth Camp 2023 is finally open.
            </p>
            <p>
              Please read the instructions carefully and fill out the form correctly in the best of your abilities.
            </p>
          </div>
          <Button href="#" className="mt-10 w-full sm:hidden">
            Get your tickets
          </Button>
        </div>
      </Container>
    </div>
  )
}
