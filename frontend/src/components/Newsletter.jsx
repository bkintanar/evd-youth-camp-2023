import Image from 'next/image'

import { Button } from '@/components/Button'
import { Container } from '@/components/Container'
import backgroundImage from '@/images/background-newsletter.jpg'

export function Newsletter() {
  return (
    <section id="newsletter" aria-label="Newsletter">
      <Container>
        <div className="relative -mx-4 overflow-hidden bg-transparent border border-gray-100 py-20 px-4 sm:-mx-6 sm:px-6 md:mx-0 md:rounded-5xl md:px-16 xl:px-24 xl:py-36">
          <div className="relative mx-auto max-w-2xl gap-x-32 gap-y-14 xl:max-w-none xl:grid-cols-2">
            <div>
              <p className="font-display text-4xl font-medium tracking-tighter text-white sm:text-5xl">
                Stay up to date
              </p>
              <p className="mt-4 text-lg tracking-tight text-gray-300">
              To stay up-to-date on the latest news and information about the UPCPI EVD YOUTH CAMP 2023, we encourage you to follow the official Facebook page of <a href="https://www.facebook.com/profile.php?id=100081188809148&mibextid=ZbWKwL" target="_blank" rel="noreferrer" className="underline">UPCPI EVD YOUTH MINISTRIES</a>. By doing so, you&apos;ll have access to all the latest announcements, event details, and other important updates related to the camp. To visit our official Facebook page, please click the link provided below. Thank you for your interest in the UPCPI EVD YOUTH CAMP 2023, and we look forward to seeing you there!
              </p>
            </div>
           
          </div>
        </div>
      </Container>
    </section>
  )
}
