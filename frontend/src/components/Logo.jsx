export function Logo(props) {
  return (
    <p className='text-white font-extrabold text-2xl font-montserrat'>EVDYOUTHCAMP<span className='font-normal text-gray-400'>2023</span></p>
  )
}
