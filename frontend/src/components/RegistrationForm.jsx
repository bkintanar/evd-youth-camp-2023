import { Container } from '@/components/Container'
import { useState } from 'react'
import { Button } from '@/components/Button'

const sections = [
    {text: 'Section 1', value: 'Section 1'},
    {text: 'Section 2', value: 'Section 2'},
    {text: 'Section 3', value: 'Section 3'},
    {text: 'Section 4', value: 'Section 4'},
    {text: 'Section 5', value: 'Section 5'},
    {text: 'Section 6', value: 'Section 6'},
    {text: 'Other', value: 'Other'},
]

const defaultForm = {
    email: '',
    first_name: '',
    last_name: '',
    middle_initial: '',
    section: '',
    local_church: '',
    host_pastor: '',
    gender: '',
    question: '',
    proof_of_payment: '',
    selected_section: '',
}

export function RegistrationForm() {
    const [formData, setFormData] = useState(defaultForm);
    const [saving, setSaving] = useState(false);
    const [success, setSuccess] = useState(false);

    const handleInput = (e) => {
        const fieldName = e.target.name;
        const fieldValue = e.target.value;

        setFormData((prevState) => ({
            ...prevState,
            [fieldName]: fieldValue
        }));

        if (fieldName === 'selected_section' && fieldValue !== 'Other') {
            setFormData((prevState) => ({
                ...prevState,
                section : fieldValue
            }));
        }

        if(fieldName === 'proof_of_payment') {
            setFormData((prevState) => ({
                ...prevState,
                proof_of_payment : e.target.files[0]
            }));
        }
    }

    const submitForm = async (e) => {
        // We don't want the page to refresh
        e.preventDefault()
      
        const data = new FormData()
      
        // Turn our formData state into data we can use with a form submission
        Object.entries(formData).forEach(([key, value]) => {
          data.append(key, value);
        })
        
        try {
            setSaving(true)
            await fetch('http://ec2-18-140-63-111.ap-southeast-1.compute.amazonaws.com/api/register', {
                method: "POST",
                body: data,
                headers: {
                    'accept': 'multipart/form-data',
                },
            })
            setSaving(false)
            setSuccess(true)
            document.getElementById("register-form").reset();
            setFormData(defaultForm)
            setInterval(function () {setSuccess(false)}, 3000);

        } catch (error) {
            setSaving(false)
            console.log(error)
        }
       
      }
    
  return (
    <section id="registration" aria-label="Registration" className="relative">
      <Container>
        <div className="py-8 lg:py-16 px-4 mx-auto max-w-screen-md">
        <h2 className="font-display text-4xl font-medium tracking-tighter text-white sm:text-5xl text-center">Registration Form</h2>
        <p className="mt-4 font-display text-2xl tracking-tight text-gray-300 mb-8 text-center">THE UPCPI EVD YOUTH CAMP 2023 REGISTRATION FORM IS A ONE-TIME SUBMISSION ONLY. ONCE YOU HAVE COMPLETED AND SUBMITTED THE FORM, YOU WILL NOT BE ABLE TO SUBMIT ANOTHER RESPONSE. HENCE, KINDLY READ THE INSTRUCTIONS CAREFULLY BEFORE FILLING OUT AND REVIEW YOUR INFORMATION UPON SUBMISSION.</p>
        <div className='mt-4 font-display text-lg tracking-tight text-white mb-8 text-left'>
            <p className="mb-2"><strong>Here&apos;s how to register:</strong></p>
            <p className='mb-5'>
            <strong>1. Fill out the registration form.</strong> Ensure that you complete the registration form in full by providing accurate information, including your full name, contact information, and any additional details that may be required.</p>

            <p><strong>2. Choose your preferred payment method: (a) Palawan or (b) Cebuana</strong></p>

            <p className='mb-5'>If paying through either Palawan or Cebuana, please send your payment to the following account details:<br/>
                Name: <strong>Richie H. Conde</strong><br/>
                Contact Number: <strong>0906 276 3549</strong></p>


            <p className='mb-5'><strong>3. After making your payment, attach a clear and legible copy of your
            proof of payment to the registration form.</strong> This should be in JPEG format for
            ease of processing. We require a copy of your proof of payment to ensure that
            your registration is processed smoothly and that you are confirmed as an attendee at the UPCPI EVD YOUTH CAMP 2023. We kindly request that you take care to ensure that the copy is clear and easy to read, as this will expedite the processing of your registration.</p>


            <p className='mb-5'><strong>4. Click submit!</strong> Once we have received your completed form and proof of
            payment, we will send you a confirmation email with further instructions.</p>

            <p className='mb-5'>Please be advised that we take the confidentiality of your personal information seriously. Any details you provide on the registration form will be treated as strictly confidential and will not be shared with any third party without your explicit consent. The UPCPI EVD Working Committee is committed to upholding the highest standards of data privacy and security, and we are dedicated to ensuring that your personal information is handled with the utmost care and respect.</p>

            <p className='mb-5'>We invite you to join us at the <strong>UPCPI EVD YOUTH CAMP 2023</strong> to grow in faith, build lasting connections, and experience the transformative power of the Holy Ghost. We are excited to welcome you and look forward to creating an unforgettable and spiritually enriching experience together.</p>

            <p><strong>#UPCPIEVDYouthCamp2023 #NowFaith</strong></p>
            
        </div>
        <form method='POST' action="#" id="register-form" className="space-y-8" onSubmit={submitForm}>
            <div>
                <label className="block mb-2 text-lg font-medium text-white ">Your email</label>
                <input name="email" value={formData.email} type="email" id="email" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="john@doe.com" required onChange={handleInput} />
            </div>
            <div>
                <p className='text-white text-lg font-medium mb-2'>Section</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'>Please select your EVD section from the selection provided.</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'>If you are a guest or attendee from another district or organization, select Other and specify your district or organization in the space provided. <strong>e.g., UPCPI AVD - Section 1</strong> </p>
                <p className='mt-1 mb-4 text-base tracking-tight text-gray-300'>This will help us ensure that you have an enjoyable and meaningful experience at the UPCPI EVD YOUTH CAMP 2023.</p>
                {sections.map((section) => (
                    <div className='flex items-center mb-4' key={section.text}>
                        <input checked={formData.selected_section === section.value} name="selected_section" id={section.text} type="radio" value={section.value} className="w-4 h-4 text-blue-600 bg-gray-100 border-blue-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600" onChange={handleInput} required/>
                        <label className="ml-2 text-sm font-medium text-white">{section.text}</label>
                    </div>
                ))}
                <input name="section" type="text" id="other" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="UPCPI AVD - Section 1" onChange={handleInput} disabled={formData.selected_section == 'Other' ? false : true} required={formData.selected_section == 'Other' ? false : true}/>
            </div>
            <div>
                <label className="block mb-2 text-lg font-medium text-white ">First Name</label>
                <input value={formData.first_name} name="first_name" type="text" id="name" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="First name" required onChange={handleInput}/>
            </div>
            <div>
                <label className="block mb-2 text-lg font-medium text-white ">Middle Initial</label>
                <input value={formData.middle_initial} name="middle_initial" type="text" id="name" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="Middle initial" onChange={handleInput}/>
            </div>
            <div>
                <label className="block mb-2 text-lg font-medium text-white ">Last Name</label>
                <input value={formData.last_name} name="last_name" type="text" id="last-name" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="Last name" required onChange={handleInput}/>
            </div>
            <div>
                <p className='text-white text-lg font-medium mb-2'>Gender</p>
                <div className="flex items-center mb-4">
                    <input checked={formData.gender === 'male' ? true : false} id="disabled-radio-1" type="radio" value="male" name="gender" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500" onChange={handleInput} required/>
                    <label className="ml-2 text-sm font-medium text-white">Male</label>
                </div>
                <div className="flex items-center">
                    <input checked={formData.gender === 'female' ? true : false} id="disabled-radio-2" type="radio" value="female" name="gender" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-900 dark:focus:ring-blue-600" onChange={handleInput} required/>
                    <label className="ml-2 text-sm font-medium text-white">Female</label>
                </div>
            </div>
            <div>
                <p className='text-white text-lg font-medium mb-2'>Name of Local Church</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'>Please provide accurate information with the following format:</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'><strong>e.g., UPC FAP - First Apostolic Pentecostal Church </strong> </p>
                <input value={formData.local_church} name="local_church" type="text" id="church" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="UPC FAP - First Apostolic Pentecostal Church" required onChange={handleInput}/>
            </div>
            <div>
                <p className='text-white text-lg font-medium mb-2'>Name of Host Pastor</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'>Please provide accurate information with the following format:</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'><strong>e.g., Pastor Juan P. Martinez</strong> </p>
                <input value={formData.host_pastor} name="host_pastor" type="text" id="host_pastor" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="e.g., Pastor Juan P. Martinez" required onChange={handleInput}/>
            </div>

            <div>
                <p className='text-white text-lg font-medium mb-2'>Proof of Payment</p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'><strong>Reminder: Attach a clear and legible copy of your proof of payment to the registration form.</strong></p>
                <p className='mt-1 mb-2 text-base tracking-tight text-gray-300'>This should be in <strong>JPEG format</strong> for ease of processing. We require a copy of your proof of payment to ensure that your registration is processed smoothly and that you are confirmed as an attendee at the UPCPI EVD YOUTH CAMP 2023. We kindly request that you take care to ensure that the copy is <strong>clear and easy to read</strong>, as this will expedite the processing of your registration.</p>
                <input name="proof_of_payment" type="file" id="proof_of_payment" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="e.g., Pastor Juan P. Martinez" required onChange={handleInput}/>
            </div>

            <div>
                <p className='text-white text-lg font-medium mb-2'>Any questions about the event?</p>
                <input value={formData.question} name="question" type="text" id="question" className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="e.g., will there be IDs provided?" onChange={handleInput}/>
            </div>
            {saving ? <Button type="button"><svg aria-hidden="true" role="status" className="inline w-4 h-4 mr-3 text-white animate-spin" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="#E5E7EB"/>
            <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentColor"/>
            </svg>
            Submitting...</Button> : <Button type="submit">Submit</Button>}
          
        </form>
        {success ? <div className="bg-green-100 rounded-md p-3 flex mt-2">
                <svg
                    className="stroke-2 stroke-current text-green-600 h-8 w-8 mr-2 flex-shrink-0"
                    viewBox="0 0 24 24"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                >
                    <path d="M0 0h24v24H0z" stroke="none" />
                    <circle cx="12" cy="12" r="9" />
                    <path d="M9 12l2 2 4-4" />
                </svg>

                <div className="text-green-700">
                    <div className="font-bold text-xl">Successfully Register</div>
                </div>
            </div> : ''}
            
        </div>
      </Container>
    </section>
  )
}
